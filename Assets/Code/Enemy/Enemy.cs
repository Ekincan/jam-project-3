﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [Header("Movement Variables:")]
    public float movementSpeed = 4f;
    public float movementAcceleration = 4f;
    public float gravityAcceleration = 15f;

    [Header("Targeting:")]
    public float targetRange = 15f;
    public float looseRange = 30f;

    public SoundTrigger killSound;

    //Movement:
    CharacterController cc;
    Vector3 velocity;
    Vector3 desiredVelocity;

    float distanceToPlayer;
    bool isTargeting;
    bool fadeIn;

    Material mat;

    Animation anim;
    NavMeshPath path;
    new AudioSource audio;

    void Awake()
    {
        cc = GetComponent<CharacterController>();
        anim = GetComponent<Animation>();
        path = new NavMeshPath();
        audio = GetComponent<AudioSource>();
        mat = GetComponentInChildren<SkinnedMeshRenderer>().material;

        StartCoroutine(HandleAI());
        StartCoroutine(SlowUpdate());
        StartCoroutine(FindBestSpawn());

        isTargeting = false;
    }

    void Update()
    {
        HandleVelocity();
    }

    void FixedUpdate()
    {
        ExecuteVelocity();

        float distortion = mat.GetFloat("_Distortion");
        if (fadeIn)
        {
            distortion += (0.5f - distortion) * Time.fixedDeltaTime * 3f;
        }
        else
        {
            distortion += (0.25f - distortion) * Time.fixedDeltaTime * 2f;
        }
        mat.SetFloat("_Distortion", distortion);
    }

    IEnumerator FindBestSpawn()
    {
        yield return new WaitForSeconds(0.111f);

        while (true)
        {
            float bestDistance = 50f;
            EnemySpawnpoint bestPoint = null;
            NavMeshPath newPath = new NavMeshPath();

            if(EnemySpawnpoint.allPoints != null)
            {
                foreach (EnemySpawnpoint es in EnemySpawnpoint.allPoints)
                {
                    yield return new WaitForSeconds(0.013f);
                    NavMesh.CalculatePath(Player.c.navMeshPosition, es.navPosition, NavMesh.AllAreas, newPath);

                    if (newPath.corners.Length < 4 && Vector3.Distance(Player.c.transform.position, es.transform.position) > 10f)
                    {
                        float currentDistance = 0;

                        for (int c = 0; c < newPath.corners.Length; c++)
                        {
                            Vector2 oldPosition = Vector2.zero;

                            if (c == 0)
                            {
                                oldPosition = new Vector2(es.navPosition.x, es.navPosition.z);
                            }
                            else
                            {
                                oldPosition = new Vector2(newPath.corners[c - 1].x, newPath.corners[c - 1].z);
                            }

                            currentDistance += Vector2.Distance(oldPosition, new Vector2(newPath.corners[c].x, newPath.corners[c].z));
                        }

                        if (currentDistance < bestDistance)
                        {
                            bestDistance = currentDistance;
                            bestPoint = es;
                        }
                    }
                }


                yield return new WaitForSeconds(0.013f);

                if (bestPoint != null && isTargeting == false)
                {
                    if (Vector3.Distance(transform.position, bestPoint.transform.position) > 15f)
                    {
                        //Test if Enemy can be seen:
                        RaycastHit hit;
                        Vector3 cameraToMe = transform.position - Camera.main.transform.position;
                        Physics.Raycast(Camera.main.transform.position, cameraToMe, out hit, 200f);

                        //Conditions:
                        bool wallInTheWay = (hit.collider == null || hit.collider.gameObject != gameObject);
                        bool outsideFOV = Vector3.Angle(Camera.main.transform.forward, cameraToMe) > 77f;
                        yield return new WaitForSeconds(0.007f);

                        if (wallInTheWay || outsideFOV)
                        {
                            //Test if Spawnpoint can be seen:
                            cameraToMe = bestPoint.transform.position - Camera.main.transform.position;
                            Physics.Raycast(Camera.main.transform.position, cameraToMe, out hit, cameraToMe.magnitude - 0.1f);

                            //Conditions:
                            wallInTheWay = hit.collider != null;
                            outsideFOV = Vector3.Angle(Camera.main.transform.forward, cameraToMe) > 77f;

                            if (wallInTheWay || outsideFOV)
                            {
                                //All Conditions are fulfilled, teleport the enemy:
                                cc.enabled = false;
                                transform.position = bestPoint.transform.position + Vector3.up * 1.2f;
                                cc.enabled = true;
                            }
                        }
                    }
                }
            }

            yield return new WaitForSeconds(0.007f);
        }
    }

    IEnumerator HandleAI()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.2f + Random.value * 0.1f);

            NavMeshHit nmh;
            NavMesh.SamplePosition(transform.position, out nmh, 1.5f, NavMesh.AllAreas);

            Vector3 pos = transform.position;

            if (nmh.hit)
            {
                pos = nmh.position;
            }

            NavMesh.CalculatePath(pos, Player.c.navMeshPosition, NavMesh.AllAreas, path);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Player.c.gameObject)
        {
            ScreenFader.c.fadeOut = false;
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
            killSound.trigger(transform.position);
        }
    }

    IEnumerator SlowUpdate()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.02f + Random.value * 0.03f);

            distanceToPlayer = Vector3.Distance(Player.c.navMeshPosition, transform.position);

            yield return new WaitForEndOfFrame();

            if(distanceToPlayer < targetRange)
            {
                isTargeting = true;
            }
            else if(isTargeting == true && distanceToPlayer > looseRange)
            {
                isTargeting = false;
            }

            fadeIn = distanceToPlayer < 5f;

            //Desired Direction:
            desiredVelocity = Vector3.zero;

            if (isTargeting)
            {
                if (path.corners.Length > 0)
                {
                    if (path.corners.Length > 1)
                    {
                        desiredVelocity = path.corners[1] - transform.position;
                    }
                    else
                    {
                        desiredVelocity = path.corners[0] - transform.position;
                    }
                }

                desiredVelocity.y = 0;
                desiredVelocity = desiredVelocity.normalized * movementSpeed; //Set speed of desired velocity

                anim.CrossFade("Walk", 0.15f);
            }
            else
            {
                anim.CrossFade("Idle", 0.2f);
            }
        }
    }

    public void HandleVelocity()
    {
        //Gravity:
        velocity.y -= Time.deltaTime * gravityAcceleration;
        if (velocity.y < -30) velocity.y = -30;
        if (cc.isGrounded && velocity.y < -7) velocity.y = -7;

        //Change Velocity:
        desiredVelocity.y = velocity.y; //Dont change vertical velocity.
        velocity += (desiredVelocity - velocity) * Time.deltaTime * movementAcceleration;
    }

    public void ExecuteVelocity()
    {
        if(desiredVelocity != Vector3.zero)
        {
            Quaternion oldRot = transform.rotation;
            transform.LookAt(transform.position + desiredVelocity);
            transform.rotation = Quaternion.Lerp(oldRot, Quaternion.Euler(0, transform.eulerAngles.y, 0), Time.fixedDeltaTime * 40f);
        }

        cc.Move(velocity * Time.fixedDeltaTime);
    }
}
