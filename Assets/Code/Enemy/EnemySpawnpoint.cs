﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawnpoint : MonoBehaviour
{
    public static List<EnemySpawnpoint> allPoints;

    [HideInInspector] public Vector3 navPosition;

    void Start()
    {
        NavMeshHit hit;
        NavMesh.SamplePosition(transform.position, out hit, 4f, NavMesh.AllAreas);
        navPosition = hit.position;

        if(allPoints == null)
        {
            allPoints = new List<EnemySpawnpoint>();
        }
        allPoints.Add(this);
    }
}
