﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DreamShard : MonoBehaviour
{

    new Light light;
    ParticleSystem ps;

    bool absorbed;

    void Awake()
    {
        light = GetComponent<Light>();
        ps = GetComponent<ParticleSystem>();

        absorbed = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == Player.c.gameObject)
        {
            if(absorbed == false)
            {
                absorbed = true;
                ps.Stop(true, ParticleSystemStopBehavior.StopEmitting);
                ShardCounter.c.AddShard();
            }
        }
    }

    void Update()
    {
        if (absorbed)
        {
            light.intensity -= Time.deltaTime * 2f;
            if (light.intensity < 0)
            {
                light.intensity = 0f;
                Destroy(gameObject);
            }
        }
    }
}
