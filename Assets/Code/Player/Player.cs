﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    public static Player c; //Single Instance

    [Header("Movement Variables:")]
    public float movementSpeed = 4f;
    public float movementAcceleration = 4f;
    public float jumpStrength = 5f;
    public float gravityAcceleration = 15f;
    public float sprintFactor = 1.5f;
    public float headBobbingSpeed = 1f;

    [Header("Sounds:")]
    public SoundTrigger footstep;
    public SoundTrigger jump;

    [Header("Runtime:")]
    public Vector3 navMeshPosition;

    //Movement:
    CharacterController cc;
    Vector3 velocity;
    float lastSoundTime;

    //Camera:
    Transform cam;
    Animation camAnim;

    void Awake()
    {
        c = this;

        cc = GetComponent<CharacterController>();
        cam = transform.Find("Camera");
        camAnim = cam.GetComponent<Animation>();
        camAnim[camAnim.clip.name].speed = 0f;

        InvokeRepeating("UpdateNavMeshPosition", 0.1f, 0.1f);
    }

    private void Update() //Frame Update
    {
        HandleMouseLook();
        HandleVelocity();
    }

    void FixedUpdate() //Physics Update
    {
        ExecuteVelocity();
    }

    public void HandleMouseLook()
    {
        //Hide Mouse:
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        //Up & Down:
        Vector3 camLocalEuler = cam.localEulerAngles;
        if (camLocalEuler.x > 180) camLocalEuler.x -= 360; //I want values from -180 to 180 instead of 0 to 360
        camLocalEuler.x = Mathf.Clamp(camLocalEuler.x - Input.GetAxis("Mouse Y"),-80,80);
        camLocalEuler.y = camLocalEuler.z = 0;
        cam.localEulerAngles = camLocalEuler;

        //Left & Right:
        Vector3 transformEuler = transform.eulerAngles;
        transformEuler.y += Input.GetAxis("Mouse X");
        transformEuler.x = transformEuler.z = 0;
        transform.eulerAngles = transformEuler;

        //Head Bobbing:
        if(cc.isGrounded)
        {
            camAnim[camAnim.clip.name].time += new Vector2(velocity.x, velocity.z).magnitude * Time.deltaTime * headBobbingSpeed;

            if(camAnim[camAnim.clip.name].time%1f > 0.9f && camAnim[camAnim.clip.name].time > lastSoundTime + 0.5f)
            {
                lastSoundTime = camAnim[camAnim.clip.name].time;
                footstep.trigger(transform.position);
            }
        }
    }

    public void HandleVelocity()
    {
        Vector3 desiredVelocity = Vector3.zero; //Desired Velocity

        //Set the direction of desired velocity:
        if (Input.GetKey(KeyCode.W))
        {
            desiredVelocity = transform.forward;
        }
        if (Input.GetKey(KeyCode.S))
        {
            desiredVelocity -= transform.forward;
        }
        if (Input.GetKey(KeyCode.D))
        {
            desiredVelocity += transform.right;
        }
        if (Input.GetKey(KeyCode.A))
        {
            desiredVelocity -= transform.right;
        }

        if(Input.GetKeyDown(KeyCode.Space) && cc.isGrounded) //Jumping if on ground:
        {
            velocity.y = jumpStrength;
        }

        //Gravity:
        velocity.y -= Time.deltaTime * gravityAcceleration;
        if (velocity.y < -30) velocity.y = -30;
        if (cc.isGrounded && velocity.y < -7) velocity.y = -7;

        //Movement Velocity:
        desiredVelocity = desiredVelocity.normalized * movementSpeed; //Set speed of desired velocity

        //Sprinting:
        if (Input.GetKey(KeyCode.LeftShift))
        {
            desiredVelocity *= sprintFactor;
            Camera.main.fieldOfView += (77f - Camera.main.fieldOfView) * Time.deltaTime * 5f;
        }
        else
        {
            Camera.main.fieldOfView += (70f - Camera.main.fieldOfView) * Time.deltaTime * 3f;
        }

        //Change Velocity:
        desiredVelocity.y = velocity.y; //Dont change vertical velocity.
        velocity += (desiredVelocity - velocity) * Time.deltaTime * movementAcceleration;
    }

    public void ExecuteVelocity()
    {
        cc.Move(velocity * Time.fixedDeltaTime);
    }

    void UpdateNavMeshPosition()
    {
        NavMeshHit nmh;
        NavMesh.SamplePosition(transform.position, out nmh, 1.5f,NavMesh.AllAreas);

        if (nmh.hit)
        {
            navMeshPosition = nmh.position;
        }
    }
}
