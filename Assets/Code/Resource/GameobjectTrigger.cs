﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class GameobjectTrigger : TriggerBase
{
    [GUIColor("GetNameColor")]
    [FilePath(ParentFolder = "Assets/Resources/Gameobjects", Extensions = "prefab")]
    [OnValueChanged("CheckName")]
    public string name;

    public override GameObject trigger(Transform parent, Vector3 position)
    {
        if (name == "") return null;

        GameObject newObject = MonoBehaviour.Instantiate<GameObject>(ResourceManager.c.getGameObject(name));
        newObject.transform.parent.SetParent(parent, true);
        newObject.transform.position = position;

        return newObject;
    }

    #region Editor Utility
    private bool nameValid = true;
    private bool checkedYet = false;
    private void CheckName()
    {
        if (name == null) return;

        string[] nameSplit = name.Split('.');
        if (nameSplit.Length > 1)
        {
            name = nameSplit[nameSplit.Length - 2];
        }
        string[] nameShlashes = name.Split('/');
        if (nameShlashes.Length > 1)
        {
            name = nameShlashes[nameShlashes.Length - 1];
        }

        checkedYet = true;
        if (name == "")
        {
            nameValid = true;
        }
        else
        {
            nameValid = false;
            foreach (GameObject ac in Resources.LoadAll<GameObject>("Gameobjects"))
            {
                if (ac.name == name)
                {
                    nameValid = true;

                    //Highlight File.
#if UNITY_EDITOR
                    UnityEditor.EditorGUIUtility.PingObject(ac);
#endif

                    return;
                }
            }
        }
    }
    private Color GetNameColor()
    {
        if (checkedYet == false)
        {
            CheckName();
        }

        if (nameValid || name == "")
        {
            return new Color(0.7f, 1f, 0.7f, 1f);
        }
        else
        {
            return new Color(1f, 0.5f, 0.5f, 1f);
        }
    }
    #endregion
}
