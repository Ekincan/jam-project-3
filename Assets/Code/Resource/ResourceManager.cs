﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager c;

    [HideInInspector] public bool loadedDirectlyIntoScene;

    //Resources:
    Dictionary<string, AudioClip> audioFiles;
    Dictionary<string, GameObject> gameObjects;
    Dictionary<string, GameObject> playerCharacters;

    void Awake()
    {
        c = this;

        reloadResources();
    }

    public void reloadResources()
    {
        audioFiles = new Dictionary<string, AudioClip>();
        foreach(AudioClip audioClip in Resources.LoadAll<AudioClip>("Audio"))
        {
            audioFiles.Add(audioClip.name, audioClip);
        }

        gameObjects = new Dictionary<string, GameObject>();
        foreach (GameObject gameObj in Resources.LoadAll<GameObject>("GameObjects"))
        {
            gameObjects.Add(gameObj.name, gameObj);
        }

        playerCharacters = new Dictionary<string, GameObject>();
        foreach (GameObject character in Resources.LoadAll<GameObject>("Characters"))
        {
            playerCharacters.Add(character.name, character);
        }
    }

    public AudioClip getAudioClip(string clipName)
    {
        try
        {
            return audioFiles[clipName];
        } catch
        {
            Debug.Log("The audio file [" + clipName + "] does not exist.");
            return null;
        }
    }

    public GameObject getPlayerCharacter(string characterName)
    {
        try
        {
            return playerCharacters[characterName];
        }
        catch
        {
            Debug.Log("The character [" + characterName + "] does not exist.");
            return null;
        }
    }

    public GameObject getGameObject(string gameobjectName)
    {
        try
        {
            return gameObjects[gameobjectName];
        }
        catch
        {
            Debug.Log("The gameobject [" + gameobjectName + "] does not exist.");
            return null;
        }
    }
}
