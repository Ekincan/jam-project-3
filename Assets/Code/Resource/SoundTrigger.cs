﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

[System.Serializable]
public class SoundTrigger : TriggerBase
{
    float lastTriggerTime;

    [Header("Sound File:")]
    [ShowIf("showOneName")]
    [GUIColor("GetNameColor")]
    [FilePath(ParentFolder = "Assets/Resources/Audio/Sounds", Extensions = "mp3,wav,ogg,aif")]
    [OnValueChanged("CheckName")]
    public string name;

    [Header("Sound Files:")]
    [ShowIf("randomSound")]
    [FilePath(ParentFolder = "Assets/Resources/Audio/Sounds", Extensions = "mp3,wav,ogg,aif")]
    [OnValueChanged("changedNames")]
    [OnInspectorGUI("FixNames")]
    public string[] names = new string[2];

    [HorizontalGroup(GroupID = "randomSounds")]
    public bool randomSound;

    [ShowIf("randomSound")]
    [FolderPath(ParentFolder = "Assets/Resources/Audio/Sounds")]
    [OnValueChanged("CheckFolder")]
    [HorizontalGroup(GroupID = "randomSounds")]
    [SuffixLabel("Folder    ",Overlay = true)]
    [HideLabel()]
    public string loadFromFolder;

    [Header("Sound Settings:")]
    [Range(0.2f,2f)]
    [OnValueChanged("changedMinPitch")]
    public float pitchMin = 0.9f;
    [OnValueChanged("changedMaxPitch")]
    [Range(0.2f, 2f)]
    public float pitchMax = 1.1f;
    [Range(0f, 1f)] public float volume = 1f;

    [Header("3D Sound Settings:")]
    public bool proximitySound = false;
    [ShowIf("proximitySound")]
    public float range = 15f;
    [ShowIf("proximitySound")]
    [Range(0f, 1f)] public float stereoFactor = 1f;


    public SoundTrigger()
    {
        
    }
    public SoundTrigger(float newVolume, params string[] args)
    {
        if(args.Length == 1)
        {
            randomSound = false;
            name = args[0];
        }else if(args.Length > 1)
        {
            randomSound = true;
            names = args;
        }

        volume = newVolume;
    }

    #region Editor Utility
    private bool nameValid = true;
    private bool checkedYet = false;
    private void CheckName()
    {
        if (name == null) return;

        string[] nameSplit = name.Split('.');
        if(nameSplit.Length > 1)
        {
            name = nameSplit[nameSplit.Length - 2];
        }
        string[] nameShlashes = name.Split('/');
        if(nameShlashes.Length > 1)
        {
            name = nameShlashes[nameShlashes.Length - 1];
        }

        checkedYet = true;
        if(name == "")
        {
            nameValid = true;
        }
        else
        {
            nameValid = false;
            foreach(AudioClip ac in Resources.LoadAll<AudioClip>("Audio/Sounds"))
            {
                if(ac.name == name)
                {
                    nameValid = true;

                    //Highlight File.
                    #if UNITY_EDITOR
                    UnityEditor.EditorGUIUtility.PingObject(ac);
                    #endif

                    return;
                }
            }
        }
    }
    private void CheckFolder()
    {
        if(loadFromFolder != "" && loadFromFolder != " " && loadFromFolder != "   ")
        {
            AudioClip[] clips = Resources.LoadAll<AudioClip>("Audio/Sounds/" + loadFromFolder);
            if (clips.Length > 1)
            {
                names = new string[clips.Length];

                for (int n = 0; n < clips.Length; n++)
                {
                    names[n] = clips[n].name;
                }

                loadFromFolder = "";
            }
        }
    }
    private Color GetNameColor()
    {
        if(checkedYet == false)
        {
            CheckName();
        }

        if (nameValid || name == "")
        {
            return new Color(0.7f, 1f, 0.7f, 1f);
        }
        else
        {
            return new Color(1f, 0.5f, 0.5f, 1f);
        }
    }
    private bool showOneName()
    {
        return !randomSound;
    }
    private void changedMaxPitch()
    {
        if(pitchMin > pitchMax)
        {
            pitchMin = pitchMax;
        }
    }
    private void changedMinPitch()
    {
        if (pitchMax < pitchMin)
        {
            pitchMax = pitchMin;
        }
    }
    private void changedNames()
    {
        if(names.Length < 2)
        {
            if(names.Length == 1)
            {
                string firstValue = names[0];
                names = new string[2];
                names[0] = firstValue;
                names[1] = "";
            }
            else
            {
                names = new string[2];
                names[0] = "";
                names[1] = "";
            }
        }

        for(int n = 0; n < names.Length; n++)
        {
            string[] nameSplit = names[n].Split('.');
            if (nameSplit.Length > 1)
            {
                names[n] = nameSplit[nameSplit.Length - 2];
            }

            string[] nameShlashes = names[n].Split('/');
            if (nameShlashes.Length > 1)
            {
                names[n] = nameShlashes[nameShlashes.Length - 1];
            }
        }
    }
    private void FixNames()
    {
        if (names == null) return;

        for (int n = 0; n < names.Length; n++)
        {
            if(names[n] != null)
            {
                string[] nameSplit = names[n].Split('.');
                if (nameSplit.Length > 1)
                {
                    names[n] = nameSplit[nameSplit.Length - 2];
                }
            }
        }
    }
    #endregion

    public override GameObject trigger(Transform parent, Vector3 position)
    {
        if(Time.time < lastTriggerTime + 0.15f)
        {
            return null;
        }
        lastTriggerTime = Time.time;

        string soundName;

        if (randomSound)
        {
            soundName = names[(int) (names.Length * Random.value * 0.999f)];
        }
        else
        {
            soundName = name;
        }

        if(soundName != "")
        {
            GameObject soundSource = new GameObject("Sound: " + soundName);
            soundSource.transform.SetParent(parent, true);
            soundSource.transform.position = position;

            AudioSource source = soundSource.AddComponent<AudioSource>();

            source.pitch = pitchMin + (pitchMax - pitchMin) * Random.value;
            source.volume = volume * AudioSettings.c.masterVolume * AudioSettings.c.soundVolume;

            if (proximitySound)
            {
                source.minDistance = 0f;
                source.maxDistance = range;
                source.spatialBlend = 1f;
                source.spread = 180f - AudioSettings.c.stereoEffect * stereoFactor * 180f;
                source.rolloffMode = AudioRolloffMode.Linear;
            }
            else
            {
                source.minDistance = 800f;
                source.maxDistance = 1000f;
            }

            source.clip = ResourceManager.c.getAudioClip(soundName);
            source.Play();

            if(source.clip != null)
            {
                MonoBehaviour.Destroy(soundSource, source.clip.length / source.pitch + 0.5f);
            }
            else
            {
                MonoBehaviour.Destroy(soundSource);
            }

            return soundSource;
        }
        else
        {
            return null;
        }
    }
}
