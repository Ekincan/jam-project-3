﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

public abstract class TriggerBase
{
    public abstract GameObject trigger(Transform parent, Vector3 position);

    public virtual GameObject trigger()
    {
        Transform cam = Camera.main.transform;
        return trigger(cam, cam.position);
    }
    public virtual GameObject trigger(Vector3 position)
    {
        return trigger(null, position);
    }
    public virtual GameObject trigger(Transform parent)
    {
        return trigger(parent, parent.position);
    }
}
