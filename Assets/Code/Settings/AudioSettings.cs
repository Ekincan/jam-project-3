﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Settings/Audio")]
public class AudioSettings : ScriptableObject
{
    public static AudioSettings c;

    [Header("Volumes:")]
    [Range(0, 1)] public float masterVolume = 0.5f;
    [Range(0, 1)] public float soundVolume = 1f;
    [Range(0, 1)] public float musicVolume = 1f;

    [Header("Other Settings:")]
    [Range(0, 1)] public float stereoEffect = 1f;
}
