﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : MonoBehaviour
{
    [Header("Settings:")]
    public new AudioSettings audio;

    void Awake()
    {
        AudioSettings.c = audio;

        //Dont Destroy:
        DontDestroyOnLoad(gameObject);
    }
}
