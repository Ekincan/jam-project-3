﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenFader : MonoBehaviour
{
    public static ScreenFader c;

    public bool fadeOut;
    public bool won;

    CanvasRenderer cr;
    CanvasGroup cgWon;
    float alpha;

    void Awake()
    {
        c = this;
        cr = GetComponent<CanvasRenderer>();
        cgWon = GetComponentInChildren<CanvasGroup>();

        cgWon.alpha = 0f;
        alpha = 1f;
        cr.SetAlpha(alpha);
        fadeOut = true;
        won = false;
    }

    void Update()
    {
        if (fadeOut)
        {
            if(Time.deltaTime < 0.07f)
            {
                alpha = Mathf.Max(alpha - Time.deltaTime, 0f);
            }

            cr.SetAlpha(alpha);
        }
        else
        {
            alpha = Mathf.Min(alpha + Time.deltaTime, 1f);
            cr.SetAlpha(alpha);

            if(alpha >= 0.99f)
            {
                if (won)
                {
                    cgWon.alpha += Time.deltaTime;
                }
                else
                {
                    EnemySpawnpoint.allPoints = null;
                    SceneManager.LoadScene(0);
                }
            }
        }
    }
}
