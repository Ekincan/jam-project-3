﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShardCounter : MonoBehaviour
{
    public static ShardCounter c;

    public SoundTrigger collectSound;
    public SoundTrigger winSound;

    TextMeshProUGUI text;
    int shards;

    void Awake()
    {
        c = this;

        text = GetComponent<TextMeshProUGUI>();
    }

    public void AddShard()
    {
        shards++;
        text.text = shards + " / 7";
        collectSound.trigger(transform);

        if (shards >= 7)
        {
            //WIN THE GAME ;)
            ScreenFader.c.won = true;
            ScreenFader.c.fadeOut = false;

            Player.c.enabled = false;
            Destroy(FindObjectOfType<Enemy>().gameObject);

            winSound.trigger(transform);
        }
    }
}
